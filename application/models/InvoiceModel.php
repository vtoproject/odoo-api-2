<?php
class InvoiceModel extends CI_Model
{
	public function getdata($id = null){
		if($id === null){
			$this->db->limit('100');	
			return $this->db->get('invoice')->result_array();
		} else {
			return $this->db->get_where('invoice',['id_invoice'=>$id])->result_array();
		}
  }

	public function deletedata($id) {
		$this->db->delete('invoice',['id_invoice' => $id]);
		return $this->db->affected_rows();
	}		
}