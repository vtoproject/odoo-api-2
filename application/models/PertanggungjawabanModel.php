<?php
class PertanggungjawabanModel extends CI_Model
{
	public function getdata($keyword = null){
		$this->db->select("*");
		$this->db->from('pertanggung_jawaban a');
		$this->db->join('pertanggung_jawaban_detail b','b.id_pertanggung_jawaban=a.id_pertanggung_jawaban','LEFT');
		if($keyword===null){
			$this->db->limit('1000');
			$this->db->order_by('a.id_pertanggung_jawaban','desc');
			$query = $this->db->get();
			return $query->result_array();
		} else {
			$this->db->where('a.id_joborder',$keyword);
			$query = $this->db->get();
			return $query->result_array();
		}
  }
}