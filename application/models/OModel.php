<?php
class OModel extends CI_Model
{
	public function getdata($table, $id = null, $id2 = null){
		switch ($table) {
			case 'pertanggung_jawaban':
				if($id === null){
					if($id2 === null){
						$this->db->limit('100');	
						return $this->db->get($table)->result_array();
					} 
				} else {
					$this->db->or_where('id',$id);
					$this->db->or_where('id_joborder',$id2);
					return $this->db->get($table)->result_array();
				}
				break;
			
			case 'invoice':
				if($id === null){
						$this->db->limit('100');	
						return $this->db->get($table)->result_array();
					} else {
						$this->db->where('no_invoice',$id);
						return $this->db->get($table)->result_array();
					}
				break;

			default:
				if($id === null){
					$this->db->limit('100');	
					return $this->db->get($table)->result_array();
				} else {
					$searchid = $this->idsearch($table);
					return $this->db->get_where($table,[$searchid => $id])->result_array();
				}					
				break;
		}		
	}

	public function deletedata($table,$id) {
		$searchid = $this->idsearch($table);
		$this->db->delete($table, [$searchid => $id]);
		return $this->db->affected_rows();
	}

	public function createdata($table,$data) {
			$this->db->insert($table,$data);
			return $this->db->affected_rows();
	} 

	public function updatedata($table,$data,$id) {
		$searchid = $this->idsearch($table);
		$this->db->update($table, $data, [$searchid => $id]);
			return $this->db->affected_rows();
	}

	function idsearch($table){
		$q = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = '".$table."' AND CONSTRAINT_NAME = 'PRIMARY' ORDER BY COLUMN_NAME ASC LIMIT 1";
		$result = $this->db->query($q);
		return $result->row()->COLUMN_NAME;
	}
}
?>