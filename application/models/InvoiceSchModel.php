<?php
class InvoiceSchModel extends CI_Model
{
	public function getdata($keyword = null){
		$this->db->select("a.tanggal, a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,d.no_aju,d.master_bl,d.no_inv_cus,d.desc_of_good,d.nm_feedervessel,d.shipping_type,d.jum_package,d.gross,d.tipe_package,d.no_feedervessel,d.port_l,d.port_d,c.*,b.invoice,d.party,f.no_con,f.tipe_cont,b.id_item,c.nama_item");
		$this->db->from('invoice a');
		$this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
		$this->db->join('item c','c.id_item=b.id_item','LEFT');
		$this->db->join('joborder d','d.id=a.id_joborder','LEFT');
		$this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
		$this->db->join('container f','f.id_joborder=d.id','LEFT');

		if($keyword===null){
			$this->db->limit('1000');
			$this->db->order_by('a.id_invoice','DESC');
			$query = $this->db->get();
			return $query->result_array();
		} else {
			$this->db->where('a.no_invoice',$keyword);
			$query = $this->db->get();
			return $query->result_array();
		}
  }
}