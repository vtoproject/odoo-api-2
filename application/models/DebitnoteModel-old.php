<?php
class DebitnoteModel extends CI_Model
{
	public function getdata($keyword = null){
		$query = $this->db->query('
		(
			SELECT a.tanggal,replace(a.`no_invoice`,"IL","DN") as no_invoice,f.`customer`,f.address,f.npwp,
			b.no_aju,b.master_bl,b.no_inv_cus,b.desc_of_good,b.no_feedervessel,b.nm_feedervessel,b.shipping_type,b.jum_package,
			b.gross,b.tipe_package,b.port_l,b.port_d,
			d.id_item,e.`nama_item`,d.currency,d.`invoice`,
			g.no_con
			FROM invoice a 
			LEFT JOIN joborder b ON b.`id`=a.`id_joborder`
			LEFT JOIN pertanggung_jawaban c ON c.`id_joborder`=b.`id`
			LEFT JOIN pertanggung_jawaban_detail d ON d.`id_pertanggung_jawaban`=c.`id_pertanggung_jawaban` 
			LEFT JOIN item e ON e.`id_item`=d.`id_item`
			LEFT JOIN customer f ON f.id_customer=b.id_customer
			LEFT JOIN container g ON g.id_joborder=b.`id`
			WHERE a.no_invoice="'.$keyword.'" AND d.`cetak`="1" AND c.`cabang`!="KPA"
			)
			UNION
			(
			SELECT m.tanggal,m.`no_invoice`,t.`customer`,t.address,t.npwp,
			p.no_aju,p.master_bl,p.no_inv_cus,p.desc_of_good,p.no_feedervessel,p.nm_feedervessel,p.shipping_type,
			p.jum_package,p.gross,p.tipe_package,p.port_l,p.port_d,
			o.`id_item`,r.`nama_item`,o.currency,o.`invoice`,
			u.no_con
			FROM invoice m 
			LEFT JOIN joborder s ON s.`id`=m.`id_joborder`
			LEFT JOIN ap n ON n.`id_joborder`=m.`id_joborder` 
			LEFT JOIN ap_detail o ON o.`id_ap`=n.`id_ap`
			LEFT JOIN joborder p ON p.`id`=m.`id_joborder`
			LEFT JOIN item r ON r.`id_item`=o.`id_item`
			LEFT JOIN customer t ON t.id_customer=s.id_customer
			LEFT JOIN container u ON u.id_joborder=s.id
			WHERE m.no_invoice="'.$keyword.'"
			)');

			// $query = $this->db->get();

		if($keyword===null){
			$this->db->limit('100');
		} else {
			$this->db->where('a.no_invoice',$keyword);
		}
		return $query->result_array();

  }
}

// $this->db->select("a.tanggal, a.`no_invoice`,e.customer,e.address,e.npwp,a.`no_pajak`,b.currency,b.invoice_kurs,d.no_aju,d.master_bl,d.no_inv_cus,d.desc_of_good,d.nm_feedervessel,d.shipping_type,d.jum_package,d.gross,d.tipe_package,d.no_feedervessel,d.port_l,d.port_d,c.*,b.invoice,f.no_con,f.tipe_cont,b.id_item,c.nama_item");
// $this->db->from('invoice a');
// $this->db->join('invoice_detail b','b.id_invoice=a.id_invoice','LEFT');
// $this->db->join('item c','c.id_item=b.id_item','LEFT');
// $this->db->join('joborder d','d.id=a.id_joborder','LEFT');
// $this->db->join('customer e','e.id_customer=d.id_customer','LEFT');
// $this->db->join('container f','f.id_joborder=d.id','LEFT');