<?php
	use chriskacerguis\RestServer\RestController;
	defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . 'libraries/RestController.php';
	require APPPATH . 'libraries/Format.php';

	class Pertanggungjawaban extends RestController {

	function __construct(){
			parent::__construct();
			$this->load->model('PertanggungjawabanModel');
			$this->methods['index_get']['limit'] = 10;
	}

	public function index_get() {
		$keyword = $this->get('id');

		if($keyword === null) {
			$query = $this->PertanggungjawabanModel->getdata();
		} else {
			$query = $this->PertanggungjawabanModel->getdata($keyword);
		}

		if($query) {
				$this->response([
						'status' => true,
						'data' => $query
				], RestController::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		} else {
				$this->response([
						'status' => false,
						'message' => 'data not found'
				], RestController::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		
		}
	}
}
?>